import React, { Component } from 'react'
import {useState, useEffect } from 'react'
import { renderMatches } from 'react-router-dom'


class HatsList extends Component {
  constructor(props){
    super(props)
    this.state = {hats: []}
  }
  async componentDidMount()
  {
    const url = 'http://localhost:8090/api/hats'
    const res = await fetch(url)
    const hatsJSON = await res.json()
    this.setState({hats: hatsJSON.hats.slice(0,100)})
  }
  async delete(id)
  {
    const deleteHatURL = `http://localhost:8090/api/hats/${id}`
    const fetchConfig = {
      method: "delete"
    }
    const deleteResponse = await fetch(deleteHatURL, fetchConfig)
    if (deleteResponse.ok){
      const deleted = await deleteResponse.json()
      console.log(deleted)
      window.location.reload(false)
    }
  }
  // const [hats, setHats] = useState([])

  // const fetchHats = async () => {
  //     const url = 'http://localhost:8090/api/hats/'
  //     const res = await fetch(url)
  //     const hatsJson = await res.json();
  //     setHats(hatsJson.hats)
  // }
  // useEffect(() => {
  //     fetchHats()
  // }, []);

  render() {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style Name</th>
            <th>Color</th>
            <th>Location</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {this.state.hats.map(hat => { 
            return (
              <tr key={ hat.id }>
                <td>{ hat.fabric }</td>
                <td>{ hat.style_name }</td>
                <td>{ hat.color }</td>
                <td>{ hat.location }</td>
                <td><button className="btn btn-danger" onClick={() => this.delete(hat.id)}>Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

}
  
  export default HatsList;