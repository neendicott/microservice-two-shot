import React from 'react';

class HatForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fabric: '',
            style_name: '',
            color: '',
            locations: [],
            };


        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({locations:data.locations});
        }
    }

    handleChange(event) {
        const value = event.target.value
        const key = event.target.name;
        const changeDict = {}
        changeDict[key] = value 
        this.setState(changeDict)
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;

        const locationUrl = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok){
            const newLocation = await response.json();
            console.log(newLocation);
            this.setState({
                fabric: '',
                style_name: '',
                color: '',
                location: '',
            });
        } 
    }


    render() {
        return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a hat</h1>
                <form onSubmit={this.handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value = {this.state.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                    <label htmlFor="name">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value = {this.state.style_name}placeholder="Style" required type="text" name="style_name" id="style_name" className="form-control" />
                    <label htmlFor="starts">Style</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value = {this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="ends">Color</label>
                </div>
                <div className="mb-3">
                    <select onChange={this.handleChange} value = {this.state.location} required name="location" id="location" className="form-select">
                    <option value="">Choose a Location</option>
                    {this.state.locations.map(location => {
                        return (
                        <option key={location.id} value={location.href}>{location.closet_name}</option>
                        )
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        );
    }
    }

    export default HatForm;