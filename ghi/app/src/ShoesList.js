
import {useState, useEffect } from 'react'
import React, { Component } from 'react'


class ShoesList extends Component {
  constructor(props){
    super(props)
    this.state = {shoes: []}
  }
  async componentDidMount()
  {
    const url = 'http://localhost:8080/api/shoes/'
    const res = await fetch(url)
    const shoesJSON = await res.json()
    this.setState({shoes: shoesJSON.shoes.slice(0,100)})
  }
  async delete(id)
  {
    const deleteShoeURL = `http://localhost:8080/api/shoes/${id}`
    const fetchConfig = {
      method: "delete"
    }
    const deleteResponse = await fetch(deleteShoeURL, fetchConfig)
    if (deleteResponse.ok){
      const deleted = await deleteResponse.json()
      console.log(deleted)
      window.location.reload(false)
    }
  }


  render() {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Color</th>
            <th>Bin</th>
            <th>Picture</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {this.state.shoes.map(shoe => { 
            return (
              <tr key={ shoe.id }>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.bin}</td>
                <td><img src={ shoe.picture_url } alt=""/></td>
                <td><button className="btn btn-danger" onClick={() => this.delete(shoe.id)}>Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
  
}
  export default ShoesList;