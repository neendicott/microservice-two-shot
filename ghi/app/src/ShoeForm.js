import React from 'react';

class ShoeForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            manufacturer: '',
            model_name: '',
            color: '',
            picture_url: '',
            bins: [],
            };


        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({bins:data.bins});
        }
    }

    handleChange(event) {
        const value = event.target.value
        const key = event.target.name;
        const changeDict = {}
        changeDict[key] = value 
        this.setState(changeDict)
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.bins;

        const binUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        
        const response = await fetch(binUrl, fetchConfig);
        if (response.ok){
            const newBin = await response.json();
            console.log(newBin);
            this.setState({
                manufacturer: '',
                model_name: '',
                color: '',
                picture_url: '',
                bin: '',
            });
        } 
    }


    render() {
        return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a pair of shoes</h1>
                <form onSubmit={this.handleSubmit} id="create-shoe-form">
                <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value = {this.state.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                    <label htmlFor="name">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value = {this.state.model_name}placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
                    <label htmlFor="starts">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value = {this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="ends">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value = {this.state.picture_url} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                    <label htmlFor="max_attendees">Picture Url</label>
                </div>
                <div className="mb-3">
                    <select onChange={this.handleChange} value = {this.state.bin} required name="bin" id="bin" className="form-select">
                    <option value="">Choose a Bin</option>
                    {this.state.bins.map(bin => {
                        return (
                        <option key={bin.id} value={bin.href}>{bin.closet_name}</option>
                        )
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        );
    }
    }

    export default ShoeForm;
