import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeForm from './ShoeForm';
import ShoesList from './ShoesList';
import HatForm from './HatForm';
import HatsList from './HatsList';

function App(props) {
  if (props.shoes && props.hats === undefined){
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" >
            <Route path="new" element={<ShoeForm/>} />
            <Route path="" element={<ShoesList shoes={props.shoes}/>} />
          </Route>
          <Route path="/hats" >
            <Route path="new" element={<HatForm/>} />
            <Route path="" element={<HatsList hats={props.hats}/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
