from django.shortcuts import render
from common.json import ModelEncoder
from django.http import JsonResponse
from .models import Hats, LocationVO
from django.views.decorators.http import require_http_methods
import json


# Create your views here.
class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "section_number", "shelf_number", "import_href"]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = ["style_name", "fabric", "color", "picture_url", "id"]
    encoders = {"location": LocationVOEncoder}

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = ["fabric", "style_name", "color"]
    encoders = {
        "location": LocationVOEncoder(),
    }

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse({"hats": hats}, encoder=HatsListEncoder)
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message":  "Invalid location id"},
                status=400,
            )
        
        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats, 
            encoder=HatsDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hats(request, pk):
    if request.method == "GET":
        try:
            hats = Hats.objects.get(id=pk)
            return JsonResponse(hats, encoder=HatsDetailEncoder, safe=False)
        except Hats.DoesNotExist:
            response = JsonResponse({"message": "Invalid hat"})
            response.status_code = 404
            return response
    
    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)
        Hats.objects.filter(id=pk).update(**content)
        hats = Hats.objects.get(id=pk)
        return JsonResponse(hats, encoder=HatsDetailEncoder, safe=False)