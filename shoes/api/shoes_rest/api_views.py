from django.http import JsonResponse
from django.shortcuts import render
from common.json import ModelEncoder
from .models import Shoes, BinVO 
from django.views.decorators.http import require_http_methods
import json
# Create your views here.


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name","import_href","id"]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = ["model_name","id","color","manufacturer","picture_url",]
    encoders = {
        "bin": BinVOEncoder(),
    }
    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}

class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = ["manufacturer","model_name","color","picture_url","id"]
    encoders = {
        "shoes": BinVOEncoder(),
    }

@require_http_methods(["GET","POST"])
def api_list_shoes(request):
    if request.method=="GET":
        shoes = Shoes.objects.all()
        return JsonResponse({"shoes": shoes},encoder=ShoesListEncoder )
    else:
        content = json.loads(request.body)
        try:
            shoes_href=content["bin"]
            shoes = BinVO.objects.get(import_href=shoes_href)
            content["bin"] = shoes
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "invalid Bin id"},status=400)
        
        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET","PUT","DELETE"])
def api_show_shoes(request, pk):
    if request.method == "GET":
        try:
            shoes = Shoes.objects.get(id=pk)
            return JsonResponse(shoes,encoder=ShoesDetailEncoder,safe=False)
        except Shoes.DoesNotExist:
            response = JsonResponse({"message":"Invalid Shoe"})
            response.status_code = 404
            return response
            
    elif request.method == "DELETE":
       count, _ = Shoes.objects.filter(id=pk).delete()
       return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Shoes.objects.filter(id=pk).update(**content)
        shoes = Shoes.objects.get(id=pk)
        return JsonResponse(
            shoes,encoder=ShoesDetailEncoder,safe=False,
        )
        # try:
        #     shoe = Shoes.objects.get(id=pk)

        #     props = ["manufacturer","model_name","color","picture_url"]
        #     for prop in props:
        #         if prop in content:
        #             setattr(shoes,prop, content[prop])
        #         shoes.save()
        #         return JsonResponse(
        #             shoes,
        #             encoder=ShoesDetailEncoder,
        #             safe=False,
        #         )
        # except Shoes.DoesNotExist:
        #     response = JsonResponse({"message": "Does not exist"})
        #     response.status_code = 404
        #     return response
